#!/usr/bin/python

import fileinput
import sys
import os

#TODO verify the arguments

switch = sys.argv[1]
#reboot = sys.argv[2]
    
fileToSearch = "/usr/share/X11/xorg.conf.d/99-fbdev.conf"

#Relaunch script with sudo if not run as sudo
##if os.geteuid() != 0:
##    os.execvp("sudo", ["sudo python"] + sys.argv)
    
if switch == '-hdmi':
    textToReplace = "fb0"
    textToSearch = "fb1"
elif switch == '-lcd':
    textToReplace = "fb1"
    textToSearch = "fb0"
else:
    print "ERROR"

#Swap Output by writing file
File = fileinput.FileInput(fileToSearch, inplace=True)

for line in File:
    print line.replace(textToSearch, textToReplace),

File.close()

#Restart Pi

#if reboot == '-r':
#    os.system('reboot')
#else:
#    print "Reboot required"
