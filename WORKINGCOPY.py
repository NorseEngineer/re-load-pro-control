#!/usr/bin/python

import sys
import serial
import datetime
import time
import threading

#Cutoff voltage in mV
UNDERVOLTAGE = 3300
#Constant current in mA
TESTCURRENT = 500
#Time to wait for a message from the Re:Load Pro in seconds
TIMEOUT = 1
#Logging invterval in sec
PERIOD = 5

sFlag = False

class SerialThread(threading.Thread):
    def __init__(self, event, ser, startTime, seconds=5.0):
        threading.Thread.__init__(self)
        self.stopped = event
        self.ser = ser
        self.startTime = startTime
        self.seconds = seconds

    def run(self):
        while not self.stopped.wait(self.seconds):
            #Call function
            self.check()

    def check(self):
        global sFlag
        elapsedTime = '{0:.1f}'.format(time.time() - self.startTime)
        
        #Read response
        ser.write('read\r'.encode())
        response = self.ser.readline()
        
        while not (not response):
            responseList = response.split()
            #Check for overtemp condition
            if responseList[0] == 'overtemp':
                print(str(elapsedTime) + ': Load too hot, terminating test')
                #Log to file
                file.write(str(elapsedTime) + ',"Load too hot, terminating test"\n')
                #End this iteration
                stopFlag.set()
                sFlag = cleanUp(self.ser, self.startTime)
                return

            #Check for a voltage too low condition
            elif responseList[0] == 'undervolt':
                print(str(elapsedTime) + ',\t,\t' + str(UNDERVOLTAGE) + '\n'),
                print('"Voltage reached critical level, terminating test"\n')
                #Log to file
                file.write(str(elapsedTime) + ': \t,\t' + str(UNDERVOLTAGE) + 'mV\n')
                file.write('"Voltage reached critical level, terminating test"\n')
                #End this iteration
                stopFlag.set()
                sFlag = cleanUp(self.ser, self.startTime)
                return
            response = self.ser.readline()

        #Flush the queue of data, if any
        ser.flushInput()
        #Query for data
        ser.write('read\r'.encode())
        
        #Read response
        response = self.ser.readline()
        responseList = response.split()
        
        if not responseList:
            pass
        #THIS IS THE NORMAL CASE
        elif responseList[0] == 'read':
            print(str(elapsedTime) + ': ' + responseList[1] + 'mA ' + responseList[2] + 'mV')
            #Log the normal conditions to file
            file.write(str(elapsedTime) + ',\t' + str(responseList[1]) + ',\t' + str(responseList[2]) + '\n')
        else:
            print(str(elapsedTime) + ': ' + response),
        return False

def cleanUp(ser, startTime):
    #Calculate the approximate capacity
    ##Actual current needs to average the actual current, not the TESTCURRENT
    hours = (time.time() - startTime)*(0.000277778)
    capacity = TESTCURRENT*hours
    #Finish up the file with the endcap information
    file.write('-------------------------------------------------------------\n')
    file.write('Elasped Time: ' + str(time.time() - startTime) + ' seconds\n')
    file.write(',,,capacity,' + str(capacity) + ',mA\n')
    file.write('Revision 0.1.1, By Aaron Norris\n')
    ser.write('version\r'.encode())
    file.write('Firmware ' + str(ser.readline()) + '\n')
    #Turn off the load
    ser.write('off\r'.encode())
    ser.write('monitor 0\r'.encode())
    ser.write('set 0\r'.encode())
    print('Load turned off\n')
    #Close the serial terminal
    ser.close()
    print('Approximate capacity: ' + str(capacity) + ' mAh')
    return True

try:
    #Ask user for the test specs
    UNDERVOLTAGE = input('Enter UVLO (mV): ')
    TESTCURRENT = input('Enter Amp (mA): ')
    SerialNumber = raw_input('Serial number: ')

    ser = serial.Serial()
    ser.port = '/dev/ttyUSB0'
    ser.baudrate = 115200
    ser.timeout = 3
    ser.open()

    #Clear the buffers, to be on the safe side
    ser.flushInput()
    ser.flushOutput()

    #Attempt to start a data flow
    ##Write an inital read command to get data flowing
    ser.write('read\r'.encode())
    print('Writing test string')
    output = ser.readline()
    lineList = output.split()
    ##Check to see if data is flowing
    if(lineList[0] != 'read'):
        print('No device responding')
        raise(Exception)
    ser.write('monitor 0\r'.encode())
    print('Device connected and communicating')

    ##Prepare the device
    #Reset all alarms and counters
    time.sleep(2)
    ser.write('reset\r'.encode())
    print('Reset: ' + str(ser.readline())),
    ser.write('reset\r'.encode())
    str(ser.readline())
    time.sleep(2)

    # #Clear not implemented, despite documentation
    # ser.write('clear\r'.encode())
    # print('Clear: ' + str(ser.readline())),

    #Set undervoltage (mV) cutoff
    ser.write('uvlo ' + str(UNDERVOLTAGE) + '\r'.encode())
    str(ser.readline())
    #Set test amperage (mA)
    ser.write('set ' + str(TESTCURRENT) + '\r'.encode())
    str(ser.readline())
    #Not ready to start the test
    ser.write('off\r'.encode())
    ser.readline()
    #Get Datetime
    dateTime = '{:%m%d%Y_%H%M%S}'.format(datetime.datetime.now())
    print('Test starting at ' + str(dateTime))
    print('Under-voltage threshold ' + str(UNDERVOLTAGE) + 'mV')
    print('Test will run at ' + str(TESTCURRENT) + 'mA')

    #Let the user know we are ready, and ask to continue
    print('Make sure DUT is connected and on')
    raw_input('Press Enter to continue...\n')
    
    #Open file
    file = open('log_' + str(dateTime) + '.log','a+')
    #Write inital conditions
    file.write('Serial No.: ' + str(SerialNumber) + '\n')
    file.write('Under-voltage setpoint: ' + str(UNDERVOLTAGE) + 'mV\n')
    file.write('Test conducted at ' + str(TESTCURRENT) + 'mA (nominal)\n')
    file.write('Logging at ' + str(PERIOD) + 'ms\n')
    file.write('-------------------------------------------------------------\n')
    file.write('"Elaspsed Time",mA,mV\n')
    #Start the load
    ser.write('on\r'.encode())
    time.sleep(1)


    #Flush the queue of data, if any
    ser.flushInput()
       
    #Read response
    ser.write('read\r'.encode())
    response = ser.readline()
    responseList = response.split()
        
    if not responseList:
        pass
    elif responseList[0] == 'read':
        print('0.0: ' + responseList[1] + 'mA ' + responseList[2] + 'mV')
        #Log the normal conditions to file
        file.write('0.0,\t' + str(responseList[1]) + ',\t' + str(responseList[2]) + '\n')

    #Get starting time
    startTime = time.time()

    #START THE TIMER HERE ---------------------------------------
    stopFlag = threading.Event()
    sThread = SerialThread(stopFlag, ser, startTime)
    sThread.daemon = True
    sThread.start()

#Keep alive the thread
    while not sFlag:
        time.sleep(1)

except KeyboardInterrupt:
    print('\nUser terminated script')
    file.write('User terminated script\n')
    ser.write('off\r'.encode())
    ser.write('monitor 0\r'.encode())
    ser.write('set 0\r'.encode())
    ser.close()
    stopFlag.set()
    print('Load off\n')

except Exception,e:
    print str(e)
    ser.close()
    print("Unknown Error, restarting")


