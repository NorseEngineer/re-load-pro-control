#!/usr/bin/python

import sys
import serial
import datetime
import time

#Cutoff voltage in mV
UNDERVOLTAGE = 3300
#Constant current in mA
TESTCURRENT = 500
#Time to wait for a message from the Re:Load Pro in seconds
TIMEOUT = 1
#Logging invterval in ms
PERIOD = 300000

while 1:
    try:
        #Ask user for the test specs
        UNDERVOLTAGE = input('Enter UVLO (mV): ')
        TESTCURRENT = input('Enter Amp (mA): ')
        SerialNumber = input('Serial number: ')
        
        ser = serial.Serial()
        ser.port = '/dev/ttyUSB0'
        ser.baudrate = 115200
        ser.timeout = 3
        ser.open()
        
        #Attempt to start a data flow
        ##Write an inital read command to get data flowing
        ser.write('read\r'.encode())
        print('Writing test string')
        output = ser.readline()
        lineList = output.split()
        ##Check to see if data is flowing
        if(lineList[0] != 'read'):
            print('No device responding')
            raise(Exception) 
        ser.write('monitor 0\r'.encode())
        print('Device connected and communicating')
        
        ##Prepare the device
        #Reset all alarms and counters
        time.sleep(2)
        ser.write('reset\r'.encode())
        print('Reset: ' + str(ser.readline())),
        ser.write('reset\r'.encode())
        str(ser.readline())
        time.sleep(2)
        
        # #Clear not implemented, despite documentation
        # ser.write('clear\r'.encode())
        # print('Clear: ' + str(ser.readline())),
        
        #Set undervoltage (mV) cutoff
        ser.write('uvlo ' + str(UNDERVOLTAGE) + '\r'.encode())
        str(ser.readline())
        #Set test amperage (mA)
        ser.write('set ' + str(TESTCURRENT) + '\r'.encode())
        str(ser.readline())
        #Not ready to start the test
        ser.write('off\r'.encode())
        ser.readline()
        #Get Datetime
        dateTime = '{:%m%d%Y_%H%M%S}'.format(datetime.datetime.now())
        print('Test starting at ' + str(dateTime))
        print('Under-voltage threshold ' + str(UNDERVOLTAGE) + 'mV')
        print('Test will run at ' + str(TESTCURRENT) + 'mA')
        
        #Let the user know we are ready, and ask to continue
        print('Make sure DUT is connected and on')
        raw_input('Press Enter to continue...\n')
        
        #Get starting time
        startTime = time.time()
        #Open file
        file = open('log_' + str(dateTime) + '.log','a+')
        #Write inital conditions
        file.write('Serial No.: ' + str(SerialNumber) + '\n')
        file.write('Under-voltage setpoint: ' + str(UNDERVOLTAGE) + 'mV\n')
        file.write('Test conducted at ' + str(TESTCURRENT) + 'mA (nominal)\n')
        file.write('Logging at ' + str(PERIOD) + 'ms\n')
        file.write('Test started at ' + str(startTime) + '\n')
        file.write('-------------------------------------------------------------\n')
        file.write('"Elaspsed Time",mA,mV\n')
        #Start the load
        ser.write('on\r'.encode())
        time.sleep(1)
        
        #Clear buffers
        #ser.reset_input_buffer()
        #ser.reset_output_buffer()
        
        #Start the data for logging
        ser.write('monitor ' + str(PERIOD) + '\r'.encode())
        
        while 1:
            response = ser.readline()
            elapsedTime = '{0:.3f}'.format(time.time() - startTime)
            responseList = response.split()
            if not responseList:
                print(response)
                pass
            #Check for overtemp condition
            elif responseList[0] == 'overtemp':
                print(str(elapsedTime) + ': Load too hot, terminating test')
                #Log to file
                file.write(str(elapsedTime) + ',"Load too hot, terminating test"\n')
                #End the loop
                break
            #Check for a voltage too low condition
            elif responseList[0] == 'undervolt':
                print(str(elapsedTime) + ': Voltage reached critical level, terminating test')
                #Log to file
                file.write(str(elapsedTime) + ',"Voltage reached critical level, terminating test"\n')
                #End the loop
                break
            elif responseList[0] == 'read':
                print(str(elapsedTime) + ': ' + responseList[1] + 'mA ' + responseList[2] + 'mV')
                #Log the normal conditions to file
                file.write(str(elapsedTime) + ',' + str(responseList[1]) + ',' + str(responseList[2]) + '\n')
            else:
                print(str(elapsedTime) + ': ' + response),

        file.write('-------------------------------------------------------------\n')
        file.write('Elasped Time: ' + str(time.time() - startTime) + ' seconds\n')
        file.write('Revision 0.1, By Aaron Norris\n')
        
        ser.write('version\r'.encode())
        file.write('Firmware ' + str(ser.readline()) + '\n')
        #Turn off the load
        ser.write('off\r'.encode())
        ser.write('monitor 0\r'.encode())
        ser.write('set 0\r'.encode())
        print('Load off\n')
        #Close the serial terminal
        ser.close()
        hours = (time.time() - startTime)*(0.000277778)
        capacity = TESTCURRENT*hours
        print('Approximate capacity: ' + str(capacity) + ' mAh')
        raw_input('Press Enter to continue...\n')
        break

    except KeyboardInterrupt:
        print('User terminated script')
        file.write('User terminated script\n')
        ser.write('off\r'.encode())
        ser.write('monitor 0\r'.encode())
        ser.write('set 0\r'.encode())
        print('Load off\n')
        break
    except Exception,e:
        print str(e)
        print("Unknown Error, restarting")
raw_input('Press Enter to Exit...\n')       
