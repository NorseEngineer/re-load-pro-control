Re:Load Software

[Arachnid Labs](http://www.arachnidlabs.com/reload-pro/) has produced an inexpensive, well built, open source, active load called  **Re:Load Pro**. This unit comes with firmware, but does not come with software.

This project has two goals: (1) Software for full control of the Re:Load Pro and (2) A roubust case that houses a fan, a Raspberry Pi, and a touchscreen.

Goal one of this project is to produce a simple GUI for interaction and scripting to extend the use of the project past the simple and limited on-board GUI. The intent is for this software to be lightweight enough and easy enough to run smoothly on a Raspberry Pi with a low resoultion TFT display, creating an all in one solution to a hobbiest's active load needs.

Goal two of this project is to create a case that can easily be printed on a variety of readily avaliable printers that mounts a Raspberry Pi and a touch screen to the Re:Load Pro, providing an all in one solution to running tests.

Current resources for code

1. <http://pyserial.readthedocs.io/en/latest/shortintro.html>

2. <https://media.readthedocs.org/pdf/pyserial/latest/pyserial.pdf>

3. <https://en.wikipedia.org/wiki/PyQt>

4. <https://wiki.python.org/moin/PyQt>

5. <https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet>

Current Software (Known) Bugs

1. The serial number won't read a string (only numbers)

2. The file output isn't a good csv format

3. The file output writes erattically

4. The set point isn't always the actual current (need to implement a PID loop of some sort to control actual v setpoint)

5. The write period to the file needs to be every 5 seconds, not every 1 seconds

Current Hardware Bugs

1. Known issue with the old cables on the unit. Turns out that the cables were not really 18AWG (much smaller) and were causing a bottleneck with max current.



